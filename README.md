# TrainerEnglishWords (customTkinter)

A simulator for learning 3000 English words, contains an SQLite base with a ready-made dictionary.

## Application features
- The entire dictionary is voiced.
- There is a filter by groups, subgroups and sets of words.
- Words can be assigned statuses as they are mastered.
- There are statistics of words by status.
- You can choose a color scheme: light or dark.


- There are several training options:
  * practice writing or word choice,
  * written or oral submission of words,
  * English-Russian or Russian-English direction of translation.



## Stack

![Python](https://img.shields.io/badge/Python-FFD43B?style=for-the-badge&logo=python&logoColor=blue)
![Python](https://img.shields.io/badge/CustomTkinter-blue?style=for-the-badge&logo=ctk&logoColor=blue) 
![Python](https://img.shields.io/badge/sqlite-FFD43B?style=for-the-badge&logo=sqlite&logoColor=blue) 


## Pictures

 ![pict1](static/1.jpg) 
 ![pict2](static/2.jpg) 
 ![pict3](static/3.jpg) 


## How to run locally

- clone repository
>`git clone git@gitlab.com:Alata9/TrainerEnglishWords.git`

- install venv & dependencies
>`python -m venv venv`\
>`python cd venv\Script`\
>`.\activate`\
>`pip install --r requirements.txt`


- start local
>`python main.py`
